package com.willianmaesato.noteapp.ui.theme

import androidx.compose.ui.graphics.Color

val LightGrey = Color(0xFFC0C0C0)
val LightBlue = Color(0xFFD7E8DE)
val RedOrange = Color(0xffffab91)
val RedPink = Color(0xfff48fb1)
val BabyBlue = Color(0xff81deea)
val Violet = Color(0xffcf94da)
val LightGreen = Color(0xffe7ed9b)